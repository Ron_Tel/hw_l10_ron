#include "Customer.h"
#include <iterator>

Customer :: Customer (std::string name) 
{
	_name = name;
}

Customer :: Customer () 
{
	_name = "###";
}

double Customer :: totalSum () const
{
	double sum = 0;
	for (auto f : _items) 
	{
		sum += f.totalPrice();
	}
	return sum;
}

//add item to set or inc units
void Customer :: addItem (Item item) 
{
	std::set<Item>::iterator it;
	it = _items.find(item);
	if (it == _items.end())
		_items.insert(item);
	else 
	{
		Item* itm = new Item(item.getName(), item.getNumber(), item.getPrice());
		itm->addItem(it->getCount() + 1);
		_items.erase(item);
		_items.insert(*itm);
	}
}

//remove item from set or dec units
void Customer :: removeItem (Item item) 
{
	std::set<Item>::iterator it;
	it = _items.find(item);
	if (it == _items.end())
		return;
	if (it->getCount() == 1)
		_items.erase(item);
	else
	{
		Item* itm = new Item(item.getName(), item.getNumber(), item.getPrice());
		itm->addItem(it->getCount() - 1);
		_items.erase(item);
		_items.insert(*itm);
	}
}

//print items by order
void Customer :: prtItems () 
{
	int a = 1;
	for (auto f : _items) 
	{
		std::cout << a << ". " << "name is : " << f.getName() << " ,";
		std::cout << "price is :" << f.totalPrice() << ", units : " << f.getCount() << "\n";
		a++;
	}
}

//return nth item on set
const Item& Customer :: getItemByNum (int num)
{
	int a = 1;
	std::set<Item>::iterator it;
	for (it = _items.begin() ; it != _items.end() ; ++it) 
	{
		if (a == num)
			return *it;
		a++;
	}
	
}

