#include <iostream>
#include <map>
#include <iterator>
#include "Customer.h"
#include "Item.h"




std::map<std::string, Customer> abcCustomers;

Item itemList[10] = {
	Item("Milk","00001",5.3),
	Item("Cookies","00002",12.6),
	Item("bread","00003",8.9),
	Item("chocolate","00004",7.0),
	Item("cheese","00005",15.3),
	Item("rice","00006",6.2),
	Item("fish", "00008", 31.65),
	Item("chicken","00007",25.99),
	Item("cucumber","00009",1.21),
	Item("tomato","00010",2.32) };

void addItem(std::string name, bool add);
bool nameExist(std::string name);
void biggestSum();

int main () 
{
	int choise = 1;
	std::string name;
	int item;
	int opt;

	//while choise != 0
	for (;choise;) 
	{
		std::cout << "welcome to the store\nplease enter option : ";
		std::cin >> choise;
		switch (choise) 
		{
		case 1:
			std::cout << "Enter your name : ";
			std::cin >> name;
			if (nameExist(name)) { std::cout << "there is such user\n"; break; }
			abcCustomers.insert({ name, Customer(name) });
			addItem(name , 1);
			break;
		case 2:
			std::cout << "Enter your name : ";
			std::cin >> name;
			if (!nameExist(name)) { std::cout << "there is no such user\n"; break; }
			std::cout << "Enter option :  ";
			std::cin >> opt;
			if (opt == 1)
				addItem(name , 1);
			if (opt == 2)
				addItem(name, 0);
			break;
		case 3:
			biggestSum();
			break;
		}

	}
	return 0;
}

//adding / removing item from customer
void addItem (std::string name , bool add) 
{
	int item;
	abcCustomers[name].prtItems();
	std::cout << "Enter item number : ";
	std::cin >> item;
	while (item && item <= 9)
	{
		add ? abcCustomers[name].addItem(itemList[item - 1]) : abcCustomers[name].removeItem(abcCustomers[name].getItemByNum(item));
		abcCustomers[name].prtItems();
		std::cout << "Enter item number : ";
		std::cin >> item;
	}
}

//check if name exist in map
bool nameExist(std::string name)
{
	for (std::map<std::string, Customer>::iterator it = abcCustomers.begin(); it != abcCustomers.end() ; ++it) {
		if (it->first == name)
			return true;
	}
	return false;
}

//find cust pay the most and prints his info
void biggestSum () 
{
	double biggest = 0;
	Customer* cust = nullptr;
	for (std::map<std::string, Customer>::iterator it = abcCustomers.begin(); it != abcCustomers.end(); ++it) {
		if (it->second.totalSum() > biggest) {
			biggest = it->second.totalSum();
			cust = &it->second;
		}
	}
	if (cust)
		std::cout << "customer pay the most is: " << cust->getName() << ", paid : " << biggest << "\n";
}