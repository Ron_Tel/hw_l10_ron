#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string name, std::string number , double price);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator<( const Item& other) const ; //compares the _serialNumber of those items.
	bool operator>( const Item& other) const ; //compares the _serialNumber of those items.
	bool operator==( const Item& other) const; //compares the _serialNumber of those items.
	
	std::string getName()  { return _name; }
	std::string getNumber() const;
	double getPrice()  { return _unitPrice; }
	void addItem(int num)  { _count = num; }
	int getCount() const { return _count; }

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};

